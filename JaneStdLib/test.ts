import { assertEquals } from "https://deno.land/std@0.130.0/testing/asserts.ts";
import { treeLookup } from "./JaneStdLib.ts";

Deno.test("round 1 - 1 level deep", () => {
  assertEquals("cat", treeLookup({ 90210: "cat" }, ["90210"], 0));
});

Deno.test("round 2 - 2 levels deeps", () => {
  assertEquals(
    "dog",
    treeLookup({ 90210: { "cat": "dog" } }, ["90210", "cat"], 0),
  );
});

Deno.test("round 3 - 2 levels deep, path does not exist", () => {
  assertEquals(
    -1,
    treeLookup({ 90210: { "cat": "dog" } }, ["90210", "mat"], -1),
  );
});

Deno.test("round 4 - 3 levels deep", () => {
  assertEquals(
    null,
    treeLookup({ "a": 1, "b": { "c": null } }, ["b", "c"], -1),
  );
});
