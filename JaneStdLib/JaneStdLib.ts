function zip<T>(arr1: string[], arr2: T[]): { [key: string]: T } {
  if (arr1.length !== arr2.length) {
    return {};
  }
  const obj: { [key: string]: T } = {};
  for (let i = 0; i < arr1.length; i++) {
    obj[arr1[i]] = arr2[i];
  }
  return obj;
}

function treeLookup(obj: any, subscripts: string[], notfound: any): any {
  let ptr = obj;
  for (const subscript of subscripts) {
    if (ptr.hasOwnProperty(subscript)) {
      ptr = ptr[subscript];
    } else {
      return notfound;
    }
  }
  return ptr;
}

export { treeLookup, zip };
