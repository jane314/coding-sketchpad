import { serve } from "https://deno.land/std@0.103.0/http/server.ts";
import {
  Form,
  FormFile,
  multiParser,
} from "https://deno.land/x/multiparser@v2.1.0/mod.ts";

const config = {
  savedir: "./",
};

function processBytes(bytes: Uint8Array): Uint8Array {
  return bytes;
}

function handleFile(bytes: Uint8Array, filename: string): Promise<void> {
  const outfilename = config.savedir + Date.now().toString() + filename;
  console.log(new Date(), "\t", outfilename);
  return Deno.writeFile(outfilename, processBytes(bytes));
}

const headers = new Headers();
headers.append("Access-Control-Allow-Origin", "*");
const s = serve({ port: 8000 });

for await (const req of s) {
  if (req.url === "/upload") {
    const form: Form | undefined = await multiParser(req);
	console.dir(form);
    if (form) {
      for (let entry of Object.values(form.files)) {
        if (!Array.isArray(entry)) {
          await handleFile(entry.content, entry.filename);
        } else {
          await Promise.all(
            entry.map((formFile: FormFile) =>
              handleFile(formFile.content, formFile.filename)
            ),
          );
        }
        await req.respond({ status: 200, body: "OK", headers: headers });
      }
    } else {
      await req.respond({ status: 500, body: "empty form?", headers: headers });
    }
  } else {
    await req.respond({
      status: 404,
      body: "not supported url",
      headers: headers,
    });
  }
}
