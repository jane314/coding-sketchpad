import { Client } from "https://deno.land/x/mysql/mod.ts";
const client = await new Client().connect({
  hostname: "localhost",
  username: "root",
  db: "test01",
  poolSize: 10, // connection limit
  password: "pass",
});

const numstr = Math.floor(Math.random() * 100000).toString().padStart(6, "0");
const str = "altieri " + new Date().toLocaleString();
let res = await client.query(`select * from table02;`);
console.log(res);
res = await client.execute(`insert into table02 (val) values ('${str}');`);
console.log(res);
await client.close();
