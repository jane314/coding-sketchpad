/*
 * mycmd -a -g  -hr --file 3 toledo "kanascity"
 */

import { parse } from "https://deno.land/std/flags/mod.ts";

enum EArgType {
  Flag,
  Setting,
  Parameter,
}

interface ICmdArg {
  argType: EArgType;
}

export default class CmdArgs {
  public constructor(usage_message: string, cmdArgs: ICmdArg[]) {
  }
}
