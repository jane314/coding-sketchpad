function concat(a, b) {
  const c = new Uint8Array(a.length + b.length);
  c.set(a);
  c.set(b, a.length);
  return c;
}

const listener = Deno.listenDatagram({
  port: 55555,
  transport: "udp",
  hostname: "74.208.207.101",
});
const lookup = {};

for await (const req of listener) {
  console.log(
    new TextDecoder().decode(req[0]).replace("\n", "") + "," +
      req[1].hostname + "," +
      req[1].port,
  );
  if (!lookup.hasOwnProperty(req[1].hostname)) {
    lookup[req[1].hostname] = {};
  }
  lookup[req[1].hostname][Number(req[1].port)] = req[1];
  for (let host in lookup) {
    for (let port in lookup[host]) {
      if (port != req[1].port) {
        listener.send(
          concat(new TextEncoder().encode(`\n${host}:${port} | `), req[0]),
          lookup[host][port],
        );
      }
    }
  }
}
