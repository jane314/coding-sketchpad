
function uint2str(uintarr) {
	return new TextDecoder('utf-8').decode(uintarr);
}

function str2uint(str) {
	return new TextEncoder('utf-8').encode(str);
}

function encrypt(uintarr) {
	return new Uint8Array( uintarr.map( n => n ^ 154 ) );
}

let str = "EVIL LURKS";

console.log( uint2str( str2uint( str ) ) );

console.log( uint2str( encrypt( encrypt( str2uint( str ) ) ) ) );
