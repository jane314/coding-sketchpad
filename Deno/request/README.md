# Bug?

Run `deno run --allow-net example.ts`, then

`curl -X POST http://localhost:50000 -d 'hello'`

On my Fedora install, I get:

```
error: Uncaught (in promise) BadResource: Bad resource ID
    at async Object.pull (deno:ext/http/01_http.js:315:24)
```
