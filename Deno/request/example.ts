import { serve } from "https://deno.land/std@0.125.0/http/server.ts";

serve((req: Request): Response => {
	req.text().then( v => console.log(v));
	return new Response('msg received');
}, { port: 50000 });

