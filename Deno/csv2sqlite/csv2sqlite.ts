import { DB } from "https://deno.land/x/sqlite/mod.ts";

// Open a database
const db = new DB("./out.db");
db.query(`
  CREATE TABLE IF NOT EXISTS out (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    a TEXT,
	b TEXT,
	c TEXT
  )
`);
db.query(`insert into out (a, b, c) values ('1', '2', '3');`);
db.query(`insert into out (a, b, c) values ('1', '2', '3');`);
db.query(`insert into out (a, b, c) values ('1', '2', '3');`);
db.query(`insert into out (a, b, c) values ('1', '2', '3');`);
console.log(db.query('select * from out;'));
