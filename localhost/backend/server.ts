
const port = 3001;

for await (const conn of Deno.listen({ port: 4500 })) {
  (async () => {
    for await (const { respondWith } of Deno.serveHttp(conn)) {
      respondWith(new Response("Hello World"));
    }
  })();
}

const handler = (req: Request): Response => {
  console.log(req);
  return new Response(new Date().toISOString(), { status: 200 });
};

console.log("I am trying to listen");
serve(handler, { port });
