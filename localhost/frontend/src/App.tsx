import { SyntheticEvent, useRef, useState } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const fileInput = useRef<HTMLInputElement>(null);
  const [filestring, setFilestring] = useState('');

  function selectFile(e: SyntheticEvent) {
    if (fileInput.current !== null && fileInput.current.files !== null) {
      setFilestring(JSON.stringify(fileInput.current.files));
	  /*
      const photo = fileInput.current.files[0];
      const formData = new FormData();
      formData.append("photo", photo);
      fetch("http://localhost:3001/upload/image", {
        method: "POST",
		mode: 'no-cors',
        //body: formData,
      }).then( res => {
		  console.log(res)
		  });
		  */
    }
  }

  return (
    <div className="App">
      <input webkitdirectory="true" onChange={selectFile} ref={fileInput} type="file" />
	  <pre>
		{filestring}
	  </pre>
    </div>
  );
}

export default App;
