#!/usr/bin/bash
basedir=$(dirname "$0")
"$basedir"/fmt.sh
rm -rfv "$basedir"/frontend/static/localhost/*
mkdir "$basedir"/frontend/localhost 2>/dev/null
cd "$basedir"/frontend/localhost
npm i
npm run build
cp -rv ./dist/* ../static/localhost
cd -
