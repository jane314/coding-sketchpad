#!/usr/bin/bash
basedir=$(dirname "$0")
find "$basedir" -type f \( -not -path '*node_modules/*' \) -and \( -name '*tsx' -or -name '*ts' \) -exec deno fmt {} \;
