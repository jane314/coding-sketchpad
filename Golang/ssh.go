package main

import (
	"bytes"
	"fmt"
	"os/exec"
	"sync"
)

func ExecRemote(cmdstr string, wg *sync.WaitGroup) (bool, error) {
	defer wg.Done()
	cmd := exec.Command("ssh", "pi", cmdstr)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return false, err
	} else {
		fmt.Println(out.String())
		return true, nil
	}
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)	
	go ExecRemote("echo a | sed -e 's/a/b/'", &wg)
	wg.Add(1)	
	go ExecRemote("echo a | sed -e 's/a/b/'", &wg)
	wg.Add(1)
	go ExecRemote("echo a | sed -e 's/a/b/'", &wg)
	wg.Wait()
}
