package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"log"
	"net"
)



func main() {
	var packet [48]byte
	packet[0] = 0x1B
	conn, _ := net.Dial("udp", "time.facebook.com:123")
	_, err := conn.Write(packet[:])
	if err != nil {
		fmt.Println(err)
		log.Fatal(1)
	}
	res := make([]byte, 2048)
	_, err = bufio.NewReader(conn).Read(res)
	if err != nil {
		fmt.Println(err)
		log.Fatal(1)
	}
	fmt.Println(binary.BigEndian.Uint32(res[40:44]) - 2208988800)
	conn.Close()
}
