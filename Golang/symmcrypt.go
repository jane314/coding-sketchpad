package main

import (
	"fmt"
)

func SymmCrypt(bytelist []byte, key string) {
	keybyte := []byte(key)
	for i := 0; i < len(bytelist); i++ {
		bytelist[i] ^= keybyte[i % len(keybyte)]
	}
}

func main() {
	bytestr := []byte("what we do in live echoes\tin eternity")
	fmt.Println(string(bytestr))
	SymmCrypt(bytestr, "evilgirl1984")
	fmt.Println(string(bytestr))
	SymmCrypt(bytestr, "evilgirl1984")
	fmt.Println(string(bytestr))
}
