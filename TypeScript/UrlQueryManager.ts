/*
 * Typescript class for making the state of a page linkable
 * To use the class, translate the state of your page into a JS object
 * Then you can make the page URL's search query parameters (https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams)
 * 		reflect the current state by calling urlQueryManager.setState(myCurrentStateObject) 
 * When the page loads, you can pull the current state by calling urlQueryManager.getState()
 */

export default class UrlQueryManager {
	
	private _url: URL;
	private _stateParamName: string;
	
	public constructor(stateParamName: string = 'state') {
		this._stateParamName = stateParamName;
		// @ts-ignore
		this._url = new URL(window.location);
	}
	
	public setState(x: Object) {
		this._url.searchParams.set(this._stateParamName, encodeURIComponent(JSON.stringify(x)));
		// @ts-ignore
		window.history.pushState({}, '', this._url);
	}
	
	public getState(): Object {
		if(!this._url.searchParams.has(this._stateParamName)) {
			console.error(`window.location (${this._url}) does not have state parameter: ${this._stateParamName}.`);
			return null;
		}
		return JSON.parse(decodeURIComponent(this._url.searchParams.get(this._stateParamName)));
	}		
	
}
