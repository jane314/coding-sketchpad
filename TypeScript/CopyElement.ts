/*
 * Copies the HTMLElement elt to the clipboard
 */
export default function copyElement(elt: HTMLElement) {
	const myrange = document.createRange();
	myrange.selectNode(elt);
	window.getSelection().removeAllRanges();
	window.getSelection().addRange(myrange);
	document.execCommand('copy');
	window.getSelection().removeAllRanges();
}
