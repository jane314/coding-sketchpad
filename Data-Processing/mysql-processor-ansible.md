
variables
- user_mysql_pass
- user_myuser_username
- user_myuser_pass
- db_name
 
```sql
create user if not exists 'janie2'@'%' identified by 'password';
set password for 'janie2'@'%' = password('pass');
grant all privileges on test01.* to 'janie2'@'%';
```

service for running jobs remote imports / remote appends / local exports on a schedule
