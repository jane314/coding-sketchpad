#!/usr/bin/env ruby
require "csv"
require "pry"
require "sqlite3"

class DataLayer
  
  def testQuery(query)
    rows = db.execute query
  end

  def tableExists(tablename)
    rows = db.execute <<~SQL, [tablename]
      SELECT name FROM sqlite_master WHERE type='table' AND name=(?);
    SQL
    rows.length > 0
  end

  def createTable(tablename, headers)
    cols = "(" + headers[0...-1].map { |col| "#{SQLite3::Database.quote col} text," }.join(" ") + " #{SQLite3::Database.quote headers[-1]}  text)"
    query = <<~SQL
      create table if not exists #{SQLite3::Database.quote tablename} #{cols} ;
    SQL
    db.execute query
  end

  def numCols(tablename)
    rows = db.execute <<~SQL, [tablename]
      select count(*) from pragma_table_info(?);
    SQL
  end

  def readCSV(csv_filepath, table, headers)
    createTable(table, headers)
    CSV.foreach csv_filepath do |row|
      puts row.join("~")
    end
  end

  # lazy class var
  def db
    @db ||=
      begin
        db = SQLite3::Database.new @db_filepath
        db.execute <<~SQL
          create table if not exists mineraldb (datetime text, post text);  
        SQL
        db
      end
  end

  def initialize(db_filepath)
    @db_filepath = db_filepath
  end
end

def main
  if ARGV.length != 2
    puts "Usage:\n\tsqlitebuddy sqlite_db_filepath table_name"
  else
    db = DataLayer.new ARGV[0]
    db.test (ARGV[1])
  end
end

main
